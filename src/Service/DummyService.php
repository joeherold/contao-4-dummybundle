<?php

namespace Pliigo\DummyBundle\Service;

class DummyService
{
	public function getResult() {
		return [
			'lorem','ipsum','dolor'
		];
	}
}