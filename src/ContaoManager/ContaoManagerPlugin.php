<?php


/*
 * This file is part of Pliigo.
 *
 * Copyright (c) 2018 Johannes Pichler
 *
 * @license LGPL-3.0+
 */

namespace Pliigo\DummyBundle\ContaoManager;


use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\ManagerPlugin\Routing\RoutingPluginInterface;
use Pliigo\DummyBundle\PliigoDummyBundle;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * ContaoManagerPlugin for the Contao Manager.
 *
 * @author Johannes Pichler <https://github.com/joeherold>
 */
class ContaoManagerPlugin implements BundlePluginInterface, RoutingPluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(PliigoDummyBundle::class)  // HERE THE BUNDLE NAME IS DEFINED
            ->setLoadAfter([ContaoCoreBundle::class])             // HERE WE DEFINE WHAT BUNDLES SHOULD BE LOADED BEFORE
            ->setReplace(['pliigo-random-subpages-bundle']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRouteCollection(LoaderResolverInterface $resolver, KernelInterface $kernel)
    {
        return $resolver
            ->resolve(__DIR__ . '/../Resources/config/routing.yml')
            ->load(__DIR__ . '/../Resources/config/routing.yml');
    }
}
