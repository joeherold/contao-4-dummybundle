<?php

/**
 * Contao Open Source CMS
 */




if(TL_MODE == 'FE') {
    // Pfad ggf. anpassen
    // Alle Dateien in /src/Ressources/public werden unter /web/bundles/bundle-name
    // als Symlink veröffentlicht nach composer install/update
    // Wie kommt der bundle name zustande? Ganz einfach:
    // Ihr ladet euer Bundle ja via ContaoManagerPlugin.php als Class.
    // Der name des Bundle Ordners ist dann der Klassenname (ganzer Name) ohne dem Wort ...Bundle und klein geschrieben.
    $GLOBALS['TL_CSS'][] = 'bundles/pliigodummy/css/dummy.css|static';
    $GLOBALS['TL_JAVASCRIPT'][] = 'bundles/pliigodummy/js/dummy.js|static';
}

if(TL_MODE == 'BE') {
    // Pfad ggf. anpassen
    // Alle Dateien in /src/Ressources/public werden unter /web/bundles/bundle-name aus der composer.json
    // als Symlink veröffentlicht nach composer install/update
    // falls ihr ein -bundle im name habt, wird das wegelassen. also aus "pliigo/dummy-bundle" wird dann pliigodummy

    $GLOBALS['TL_CSS'][] = 'bundles/pliigodummy/backend/css/backend.css|static';
    $GLOBALS['TL_JAVASCRIPT'][] = 'bundles/pliigodummy/backend/js/backend.js|static';
}

array_insert($GLOBALS['TL_CTE']['texts'],2,array (
    'content_dummy' => 'Pliigo\DummyBundle\ContentElement\ContentDummy',
));