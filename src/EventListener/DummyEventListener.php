<?php


namespace Pliigo\DummyBundle\EventListener;

use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Adds the calendar feeds to the page header.
 *
 * @author Andreas Schempp <https://github.com/aschempp>
 * @author Leo Feyer <https://github.com/leofeyer>
 */
class DummyEventListener
{


    /**
     * @var ContaoFrameworkInterface
     */
    private $framework;

    private $container;


    /**
     * Constructor.
     *
     * @param ContaoFrameworkInterface $framework
     * @param ContainerInterface $container
     */
    public function __construct(ContaoFrameworkInterface $framework, ContainerInterface $container)
    {
        $this->framework = $framework;
        $this->container = $container;
    }


    public function onKernelRequest(GetResponseEvent $event)
    {
    }

    /**
     * Adds the Contao headers to the Symfony response.
     *
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (!$this->framework->isInitialized()) {
            return;
        }

        global $objPage;


        $response = $event->getResponse();

        // $event->setResponse($aResponse);

    }
}
