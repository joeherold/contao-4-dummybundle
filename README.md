# Contao 4.x: Pliigo Dummy Bundle
This extension is ment to be a starting point for developing contao extension for contao starting from version 4.4 and up.
In the beginning, I would like to give a short overview, of the bundle structure and how it is set up, so that contao
understands this bundle as a contao bundle an instantiates and registeres all nessescary stuff.

If you have any questions, feel free to contact me via email.
- My Name: *Johannes Pichler*
- My Organisation: *Webpixels*
- My Email: *[j.pichler@webpixels.at](mailto:j.pichler@webpixels.at)*
- My Site: *[webpixels.at](https://webpixels.at)*

## DummyBundle Explanation
We well cover in short the structure of a contao bundle here and how this bundle is read by Contao using Symfony

The order in what the extension is read and build is the order I will describe the bundle here in short:

1. composer require pliigo/dummy-bundle (also done via the manager web-interface) reads the composer.json
2. composer.json has an extra tag ```extra -> contao-manager-plugin -> ContaoManagerPlugin```
3. ContaoManagerPlugin is loaded
4. ContaoManagerPlugin->getBundles() is executed
    1. BundleName is defined
5. ContaoManagerPlugin->getRouteCollection() is executed
    1. routes.yml is laoded
6. loading of BundleClass defined in ContaoManagerPlugin->getBundles()
7. BundleClass->getContainerExtension() is executed
8. Extension Class is instantiated via new keyword
9. Extension->load() is executed
    1. services.yml is loaded
    2. listeners.yml is loaded

### Prolog
Composer.json and Composer stuff is explained in the end of this readme file.


### a) Bundle Initalisation via Contao Manager:
First is to be said, that the Contao Manager is the main entry point of your plugin and does 2 things in general:
1. it defines the Bundle
2. it registers routes to be used with contao

#### a) 1. Bundle Definition
Name of the Bundle is the Class Name of the PHP-Class, loaded by ContaoManagerPlugin.php (src/ConaoManager/ContaoManagerPlugin.php Line 38)
So, the initial part is, that the Contao managed edition loads the ContaoManagerPlugin.php and calls the function *getBundles()* what returns an array of Bundle-Definitions.
How the initialisation works, we will see in b).
```php
<?php

namespace Pliigo\DummyBundle\ContaoManager;


use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\ManagerPlugin\Routing\RoutingPluginInterface;
use Pliigo\DummyBundle\PliigoDummyBundle;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * ContaoManagerPlugin for the Contao Manager.
 *
 * @author Johannes Pichler <https://github.com/joeherold>
 */
class ContaoManagerPlugin implements BundlePluginInterface, RoutingPluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(PliigoDummyBundle::class) // HERE THE BUNDLE NAME IS DEFINED
                ->setLoadAfter([ContaoCoreBundle::class]) // HERE WE DEFINE WHAT BUNDLES SHOULD BE LOADED BEFORE
                ->setReplace(['pliigo-random-subpages-bundle']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRouteCollection(LoaderResolverInterface $resolver, KernelInterface $kernel)
    {
        return $resolver
            ->resolve(__DIR__ . '/../Resources/config/routing.yml')
            ->load(__DIR__ . '/../Resources/config/routing.yml');
    }
}
```

#### a) 2. Register Routes
The Contao Manager implements the **RoutingPluginInterface** Interface. So we can provide a **getRouterCollection()**
function so that the Contao Symfony Application knows what routing files to load and initialize.

```php
<?php

namespace Pliigo\DummyBundle\ContaoManager;

...

/**
 * ContaoManagerPlugin for the Contao Manager.
 *
 * @author Johannes Pichler <https://github.com/joeherold>
 */
class ContaoManagerPlugin implements BundlePluginInterface, RoutingPluginInterface
{
    ...
    
    /**
     * {@inheritdoc}
     */
    public function getRouteCollection(LoaderResolverInterface $resolver, KernelInterface $kernel)
    {
        return $resolver
            ->resolve(__DIR__ . '/../Resources/config/routing.yml')
            ->load(__DIR__ . '/../Resources/config/routing.yml');
    }
    ...
}
```


### b) Initialization and Instantiation of the Extension defined in the Contao Manager:
The System now will initialize the bundle by the Bundle-Name defined in the Contao Manager before.
The Bundle-Name was the Class Name **PliigoDummyBundle**, so it will load the **PliigoDummyBundle** Class, that is of Type Bundle of Symfony *(Symfony\Component\HttpKernel\Bundle\Bundle)*
(In Contao Manager ```BundleConfig::create(PliigoDummyBundle::class) /* Line 38 */```)
To do so, ith instantiates the Class via the **new** keyword and returns it in the **getContainerExtension()** method.
```php
<?php

namespace Pliigo\DummyBundle;

use Pliigo\DummyBundle\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle as SymfonyBundle;

class PliigoDummyBundle extends SymfonyBundle
{

    /**
     * Builds the bundle.
     *
     * It is only ever called once when the cache is empty.
     *
     * This method can be overridden to register compilation passes,
     * other extensions, ...
     *
     * @param ContainerBuilder $container A ContainerBuilder instance
     */
    public function build(ContainerBuilder $container)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        return new Extension(); // HERE WE INSTANTIATE THE EXTENSION
    }

}
```

### c) What the Instantiated Extension does
The Extension class extends of the [Symfony Extension class](https://github.com/symfony/symfony/blob/v3.4.9/src/Symfony/Component/DependencyInjection/Extension/Extension.php).
For easier reading, we Aliased the Extension Class of Symfony to SymfonyExtension, so we can name our class just Extension.
What we do here is, that we load (except routings) the configuration files of our application.
This is done via ```$loader->load('services.yml');``` and ```$loader->load('listener.yml');``` in the load method.
So now, Contao will know, what and how we are configuring services and listeners.
```php
<?php

namespace Pliigo\DummyBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension as SymfonyExtension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class Extension extends SymfonyExtension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $mergedConfig, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config')
        );

        $loader->load('services.yml');
        $loader->load('listener.yml');
    }
}

```
in the Configuration files located at ```DummyBundle/src/Resources/config/*.yml``` all the Dependency Injection will take place.
Easy Solutions IT made a neat explanation to Dependency Injection in Contao 4 using Symfony and can be read here: [Contao 4, Dependency Injection](https://easysolutionsit.de/artikel/contao-4-dependency-injection.htmls) by easysolutionsit.de
So I will not cover the dependency injection itself here.



### d) Develop as you where used to in Contao 3.5.x

So from now on, we can do the yet well known way of developing in Contao in the way of Contao 3.5 was already done.
The *Resources* Folder contains a folder named contao. In there you have your regular templates folder, dca folder, contao config folder with config.php and so on.
So nothing really new here.

The only thing, that is rather different is, that you don't have a autoload.ini and autoload.php. Why? Because the laoding of namespaces and php Classes is handled by composer, so nothing to do so far for us.
We will take a look to composer.json later here.

### e) Wait, the public stuff (former assets folder in your plugins) and how it is linked to the public web folder.
Ok, in contao 3 Modules, we usually had an *assets* folder with an *.htaccess* file where we made public assets like Javascript ans CSS
files available.
So, now this i not so any more. Now the folder */src/Ressources/public* is sym-linked as a sub-folder to the public web folder (/web/bundles/bundle-name)
! Very Important !
There is some Symfony Magic going on here and a naming convention. Your bundle's public folder is linked to /web/bundles/*bundle-name*.
But what is the *bundle-name*? In Chapter a) 1.) we learned, how the Bundle name is defined (in our case PliigoDummyBundle).
Now, Symfony takes that BundleName and lowercases it and removes the trainling String "Bundle".
So, illustrative what happens: ```PliigoDummyBundle ==(lowercasing)==> pliigodummybundle ==(removing bundle from string)==> pliigodummy```

So, ```PliigoDummyBundle``` becomes ```pliigodummy``` and the folder ```TL_ROOT/web/bundles/pliigodummy``` will be created and symlinked to your public folder.

So when you e.g. want to add assets in your ```contao/config.php``` you can do it that way:

```php
<?php

if(TL_MODE == 'FE') {
    // Pfad ggf. anpassen
    // Alle Dateien in /src/Ressources/public werden unter /web/bundles/bundle-name
    // als Symlink veröffentlicht nach composer install/update
    // Wie kommt der bundle name zustande? Ganz einfach:
    // Ihr ladet euer Bundle ja via ContaoManagerPlugin.php als Class.
    // Der name des Bundle Ordners ist dann der Klassenname (ganzer Name) ohne dem Wort ...Bundle und klein geschrieben.
    $GLOBALS['TL_CSS'][] = 'bundles/pliigodummy/css/dummy.css|static';
    $GLOBALS['TL_JAVASCRIPT'][] = 'bundles/pliigodummy/js/dummy.js|static';
}

if(TL_MODE == 'BE') {
    // Pfad ggf. anpassen
    // Alle Dateien in /src/Ressources/public werden unter /web/bundles/bundle-name aus der composer.json
    // als Symlink veröffentlicht nach composer install/update
    // falls ihr ein -bundle im name habt, wird das wegelassen. also aus "pliigo/dummy-bundle" wird dann pliigodummy

    $GLOBALS['TL_CSS'][] = 'bundles/pliigodummy/backend/css/backend.css|static';
    $GLOBALS['TL_JAVASCRIPT'][] = 'bundles/pliigodummy/backend/js/backend.js|static';
}

array_insert($GLOBALS['TL_CTE']['texts'],2,array (
    'content_dummy' => 'Pliigo\DummyBundle\ContentElement\ContentDummy',
));
```


## composer.json explanation
The composer json is rather simple

First an overview of the whole file. Than we well look  into the different parts and what they mean

```json
{
  "name": "pliigo/dummy-bundle",
  "type": "contao-bundle",
  "description": "Starter Bundle for Contao Plugin development. Please replace DummyBundle in whole Project with your Bundle-Name" ,
  "license": "LGPL-3.0+",
  "version": "1.0.1",
  "keywords":["contao", "plugin"],
  "authors": [
    {
      "name": "Johannes Pichler",
      "homepage": "https://webpixels.at",
      "email": "j.pichler@webpixels.at"
    }
  ],
  "require": {
    "php": "^5.6|^7.0",
    "symfony/framework-bundle": "^2.8|^3.0",
    "contao/core-bundle": "^4.5|^4.4"
  },
  "conflict": {
    "contao/core": "*",
    "contao/manager-plugin": "<2.0 || >=3.0"
  },
  "require-dev": {
    "contao/manager-plugin": "^2.0",
    "friendsofphp/php-cs-fixer": "^2.6"
  },
  "autoload": {
    "psr-4": {
      "Pliigo\\DummyBundle\\": "src/"
    },
    "classmap": [
      "src/Resources/contao/"
    ]
  },
  "config": {
    "preferred-install": "dist"
  },
  "extra":{
    "branch-alias": {
      "dev-develop": "4.4.x-dev"
    },
    "contao-manager-plugin": "Pliigo\\DummyBundle\\ContaoManager\\ContaoManagerPlugin"
  }
}
```

### the main information in the beginning 

```json
{
  "name": "pliigo/dummy-bundle",
  "type": "contao-bundle",
  "description": "Starter Bundle for Contao Plugin development. Please replace DummyBundle in whole Project with your Bundle-Name" ,
  "license": "LGPL-3.0+",
  "version": "1.0.1",
  "keywords":["contao", "plugin"],
  ...
}
```
*name* is the vendor and bundle identifire of your plugin. That is also the name, you will find the plugin in the Contao Manger, after you published it to [packagist.org](https://packagist.org).


```json
{
  ...
  "authors": [
    {
      "name": "Johannes Pichler",
      "homepage": "https://webpixels.at",
      "email": "j.pichler@webpixels.at"
    }
  ],
  ...
}
```

TO BE CONTINUED...

## Full explanation
Christian Schiffler made a neat PDF Document, with a full explanation of developling an Contao 4 Extension

PDF: [Contao 4 Extension from Scratch](https://www.cyberspectrum.de/talks.html?file=files/downloads/talks/c4extension_cnt2017.pdf)
